# -*- coding: utf-8 -*-
"""
Created on Wed Jun 26 12:49:04 2019

@author: fc469
"""

# =============================================================================
# The complete list of commands for the Agilent oscilloscope can be found at:
# https://www.keysight.com/upload/cmc_upload/All/2000_series_prog_guide.pdf
# =============================================================================

import visa
import numpy as np
import matplotlib.pyplot as plt
import csv
import time

# =============================================================================
NUM_OF_CYCLES = 5
NUM_OF_POINTS = 1000
SOURCE = 'CHAN2' #choose CHAN1/CHAN2/MATH
# =============================================================================

#Setting the connection
rm = visa.ResourceManager()
my = rm.open_resource('USB0::0x0957::0x179A::MY52161403::INSTR')
my.write_termination = '\n'
my.read_termination ='\n'

#Set source
my.write(":WAVeform:SOURce " + SOURCE)

#Measurement control
my.write(":WAVeform:POINts " + str(NUM_OF_POINTS))
my.write(":WAVeform:FORMat ASCii")

#Create average array
data = my.query(":WAVeform:DATA?")
data=data[10:]
data= data.split(',')
average = np.zeros(len(data))

for i in range(NUM_OF_CYCLES):
    
    data = my.query(":WAVeform:DATA?")
    #Data processing
    data=data[10:]
    data= data.split(',')
    data= [float(x) for x in data]
    
    data= np.array(data)
    average += data
    
average = average/NUM_OF_CYCLES

#Timescale
preamble = my.query(":WAVeform:PREamble?").split(',')
preamble = [float(x) for x in preamble]
t_increment = preamble[4]
time_data = np.array(list(range(len(data))))*t_increment


#Create CSV file
file_name = 'Oscilloscope measurement_' + time.strftime("%Y%m%d-%H%M%S")+ '.csv'
            
rows = list(zip(time_data,data))

with open(file_name, 'w',newline='') as file:
    writer= csv.writer(file)
    for row in rows:
        writer.writerow(row)

#Plotting
plt.plot(time_data,average)
plt.show()
